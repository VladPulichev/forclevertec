//
//  GetSendData.swift
//  ForClevertec
//
//  Created by Владислав Пуличев on 25.03.17.
//  Copyright © 2017 Владислав Пуличев. All rights reserved.
//

import UIKit

class GetSendData {
    var delegate: MainTableViewDataDelegate?
    var delegateResults: ResultsDelegate?
    var mainViewController: MainViewController!
    
    func getData(mainViewController: MainViewController) {
        //sleep(2) // data getting is too fast. No time to cancell the operation.
        self.mainViewController = mainViewController
        var request = URLRequest(url: URL(string: "http://test.clevertec.ru/tt/meta")!)
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            self.tryToSerializeJSONFromServer(dataToSerialize: data)
        }
        task.resume()
    }
    
    func tryToSerializeJSONFromServer(dataToSerialize: Data) {
        var fieldsToReturn = [DataField]()
        do {
            let json = try JSONSerialization.jsonObject(with: dataToSerialize) as! [String: Any]
            
            let dataTitleText = json["title"] as? String
            
            let fields = json["fields"] as? Array<[String: Any]>
            for field in fields! {
                let newDataField = DataField(fields: field)
                fieldsToReturn.append(newDataField)
            }
            // passing data to main table
            if let del = delegate {
                del.dataChanged(dataTitleText: dataTitleText!, dataFields: fieldsToReturn)
            }
        } catch {
            print(error)
        }
    }
    
    func sendData(jsonData: [String: Any]) {
        let json = try? JSONSerialization.data(withJSONObject: jsonData)
        
        var request = URLRequest(url: URL(string: "http://test.clevertec.ru/tt/data/")!)
        request.httpMethod = "POST"
        // insert json data to the request
        request.httpBody = json
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            print(response!)
            self.tryToSerializeJSONResult(dataToSerialize: data)
        }
        task.resume()
    }
    
    func tryToSerializeJSONResult(dataToSerialize: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: dataToSerialize) as! [String: Any]
            
            let dataResultText = json["result"] as? String
            
            // send results to mainViewController
            if let del = delegateResults {
                del.resultsCame(results: dataResultText!)
            }
        } catch {
            print(error)
        }
    }
}
