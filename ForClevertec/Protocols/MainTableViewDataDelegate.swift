//
//  MainTableViewDataDelegate.swift
//  ForClevertec
//
//  Created by Владислав Пуличев on 26.03.17.
//  Copyright © 2017 Владислав Пуличев. All rights reserved.
//

protocol MainTableViewDataDelegate {
    func dataChanged(dataTitleText: String, dataFields: [DataField])
}
