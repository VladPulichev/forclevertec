//
//  MainTableViewCell.swift
//  ForClevertec
//
//  Created by Владислав Пуличев on 25.03.17.
//  Copyright © 2017 Владислав Пуличев. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {
    var name = UILabel()
    var field = UIView()
    var type: Int! // 1 - text, 2 - numeric, 3 - list
    var dataField: DataField! {
        didSet {
            switch dataField.type {
            case "TEXT": self.type = 1
            case "NUMERIC": self.type = 2
            case "LIST": self.type = 3
            default:
                print("Unknown type of field")
            }
        }
    }
    
    var cellWidth: CGFloat!
    var cellHeight: CGFloat!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.cellWidth = self.bounds.width
        self.cellHeight = self.bounds.height
        
        self.contentView.addSubview(name)
        self.contentView.addSubview(field)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        name.frame = CGRect(x: 10, y: 10, width: self.cellWidth * 0.5, height: self.cellHeight)
        name.numberOfLines = 2
        name.text = self.dataField.title
        
        switch type {
        case 1, 2: self.accessoryView = textField // for text and numeric types
        case 3: self.accessoryView = pickerView   // for list type
        default: print("Unknown type")
        }
    }
    
    func getFieldName() -> String {
        return self.dataField.name
    }
    
    func getFieldValue() -> String {
        switch type {
        case 1, 2:
            return self.textField.text! // for text and numeric types
        case 3:
            return Array((dataField.values)!)[pickerView.selectedRow(inComponent: 0)].value as! String   // for list type
        default:
            return "Unknown type"
        }
    }
    
    // textField for string and number types of field
    lazy var textField: UITextField! = {
        let view = UITextField(frame: CGRect(x: self.cellWidth * 0.5, y: 0, width: self.cellWidth * 0.5, height: self.cellHeight))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.borderStyle = .roundedRect
        view.textAlignment = .center
        
        return view
    }()
    
    // pickerView for chosing value type of field
    lazy var pickerView: UIPickerView! = {
        let view = UIPickerView(frame: CGRect(x: self.cellWidth * 0.5, y: 0, width: self.cellWidth * 0.5, height: self.cellHeight*2))
        view.delegate = self
        view.dataSource = self
        
        return view
    }()
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (dataField.values?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let value = Array((dataField.values)!)
        return value[row].value as? String
    }
}
