//
//  Data.swift
//  ForClevertec
//
//  Created by Владислав Пуличев on 25.03.17.
//  Copyright © 2017 Владислав Пуличев. All rights reserved.
//

struct DataField {
    var title: String!
    var name: String!
    var type: String!
    var values: [String: Any]? // for List type
    
    init(fields: [String: Any]) {
        self.title = fields["title"]! as! String
        self.name = fields["name"]! as! String
        self.type = fields["type"]! as! String
        
        if self.type == "LIST" {
            self.values = fields["values"] as? [String: Any]
        }
    }
}
