//
//  MainViewController.swift
//  ForClevertec
//
//  Created by Владислав Пуличев on 24.03.17.
//  Copyright © 2017 Владислав Пуличев. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let cellReuseIdendifier = "mainTableViewCell"
    
    var fields = [DataField]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: .zero) // deleting empty rows
        
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: cellReuseIdendifier)
        
        view.addSubview(mainButton)
        view.addSubview(self.activityIndicator)
    }
    
    // Title
    lazy var dataTitle: UILabel! = {
        let screenSize: CGRect = UIScreen.main.bounds
        let view = UILabel(frame: CGRect(x: 0, y: 50, width: screenSize.width, height: 50))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        
        return view
    }()
    
    // main button for getting and sending data
    lazy var mainButton: UIButton! = {
        let screenSize: CGRect = UIScreen.main.bounds
        let view = UIButton(frame: CGRect(x: screenSize.width * 0.1, y: screenSize.height * 0.5 + 180,
                                          width: screenSize.width * 0.8, height: 30))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(MainViewController.mainButtonPressed), for: .touchDown)
        view.setTitle("Get data!", for: .normal)
        view.backgroundColor = UIColor.darkGray
        return view
    }()
    
    // for indicating of get and send process
    lazy var activityIndicator: UIActivityIndicatorView! = {
        let screenSize: CGRect = UIScreen.main.bounds
        let view = UIActivityIndicatorView(frame: CGRect(x: screenSize.width * 0.1, y: screenSize.height * 0.5 + 120,
                                                         width: screenSize.width * 0.8, height: 40))
        view.activityIndicatorViewStyle = .gray
        return view
    }()
    
    // results
    lazy var results: UILabel! = {
        let screenSize: CGRect = UIScreen.main.bounds
        let view = UILabel(frame: CGRect(x: 0, y: screenSize.height * 0.5 + 180 + 30, width: screenSize.width, height: 70))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.numberOfLines = 2
        view.textColor = UIColor.blue
        return view
    }()
    
    func mainButtonPressed() {
        switch self.mainButton.titleLabel!.text! {
        case "Get data!":
            self.mainButton.setTitle("Cancel!", for: .normal)
            self.activityIndicator.startAnimating()
            
            let serverSide = GetSendData()
            serverSide.delegate = self // for data fields getting
            serverSide.getData(mainViewController: self) // sending self to check cancelling
            
        case "Send data!":
            let serverSide = GetSendData()
            serverSide.delegateResults = self // for results getting
            // get ready data for sending to server
            var jsonData = [String: Any]()
            var values = [String: String]()
            
            var i = 0
            while i < fields.count { // for each cell
                let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! MainTableViewCell
                values[cell.getFieldName()] = cell.getFieldValue()
                i += 1
            }
            
            jsonData["form"] = values
            
            serverSide.sendData(jsonData: jsonData) // sending self to check cancelling
        case "Cancel!": // i havent implemented it yet
            self.activityIndicator.stopAnimating()
            self.mainButton.setTitle("Get data!", for: .normal)
        default: print("Unknown button state")
        }
    }
    
    // table view for data fields
    lazy var tableView: UITableView! = {
        let screenSize: CGRect = UIScreen.main.bounds
        let view = UITableView(frame: CGRect(x: 0, y: 120, width: screenSize.width,
                                             height: screenSize.height * 0.5), style: UITableViewStyle.plain)
        
        return view
    }()
    
    // MARK: - UITableViewDataSource and UITableViewDelegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellReuseIdendifier, for: indexPath) as! MainTableViewCell
        let row = indexPath.row
        cell.dataField = fields[row]
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fields.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 // should be enough
    }
}

extension MainViewController: MainTableViewDataDelegate {
    func dataChanged(dataTitleText: String, dataFields: [DataField]) {
        DispatchQueue.main.async {
            self.dataTitle.text = dataTitleText
            self.fields = dataFields
            
            self.view.addSubview(self.tableView)
            self.view.addSubview(self.dataTitle)
            
            self.activityIndicator.stopAnimating()
            self.mainButton.setTitle("Send data!", for: .normal)
        }
    }
}

extension MainViewController: ResultsDelegate {
    func resultsCame(results: String) {
        DispatchQueue.main.async {
            self.results.text = results
            self.view.addSubview(self.results)
        }
    }
}
